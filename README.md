# DNShare

*Free, temporary, and anonymous. A-host registration.*

A simple site to create DNS A-records anonymously; something like FreeDNS, but without the need for registration.

## Screenshots

![DNShare Home Picture](docs/dnshare_2.png)

![DNShare Claim Anonymous Registration](docs/dnshare_6.png)
