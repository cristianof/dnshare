import os


def parse_bool(text):
    return text.lower() == "true"


def get_env(name, parser=None):
    raw_value = os.environ.get(name, default=None)
    if not raw_value:
        raise KeyError(f'Unable to load "{name}" from environment.')

    return parser(raw_value) if parser else raw_value


SECRET_KEY = get_env('SECRET_KEY')
SQLALCHEMY_DATABASE_URI = get_env('SQLALCHEMY_DATABASE_URI')
SQLALCHEMY_TRACK_MODIFICATIONS = get_env('SQLALCHEMY_TRACK_MODIFICATIONS',
                                         parse_bool)
DAILY_REGISTRATION_LIMIT = get_env('DAILY_REGISTRATION_LIMIT', int)
NUM_PROXIES = get_env('NUM_PROXIES', int)

MAIL_SERVER = get_env('MAIL_SERVER')
MAIL_PORT = get_env('MAIL_PORT', int)
MAIL_USE_TLS = get_env('MAIL_USE_TLS', parse_bool)  # STARTTLS extension
MAIL_USE_SSL = get_env('MAIL_USE_SSL', parse_bool)
MAIL_USERNAME = get_env('MAIL_USERNAME')
MAIL_PASSWORD = get_env('MAIL_PASSWORD')
MAIL_DEFAULT_SENDER = get_env('MAIL_DEFAULT_SENDER')
