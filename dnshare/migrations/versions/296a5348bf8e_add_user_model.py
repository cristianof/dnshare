"""Add User model

Revision ID: 296a5348bf8e
Revises: 7010dbbc5b5a
Create Date: 2018-10-01 01:46:30.425707

"""
import os
import sys

import sqlalchemy as sa
from alembic import op

sys.path.append(os.getcwd())
import utilities  # noqa

# revision identifiers, used by Alembic.
revision = '296a5348bf8e'
down_revision = '7010dbbc5b5a'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table('user',
                    sa.Column('id', sa.Integer(), nullable=False),
                    sa.Column('email', sa.String(length=255), nullable=False),
                    sa.Column('password', sa.Text(), nullable=False),
                    sa.Column('is_active', sa.Boolean(), server_default='1', nullable=False),
                    sa.Column('created_ip', sa.String(length=45), nullable=True),
                    sa.Column('created_date', utilities.util_sqlalchemy.TzAwareDateTime(),
                              nullable=True),
                    sa.Column('last_sign_in_ip', sa.String(length=45), nullable=True),
                    sa.Column('last_sign_in_date', utilities.util_sqlalchemy.TzAwareDateTime(),
                              nullable=True),
                    sa.PrimaryKeyConstraint('id')
                    )
    op.create_index(op.f('ix_user_email'), 'user', ['email'], unique=True)


def downgrade():
    op.drop_index(op.f('ix_user_email'), table_name='user')
    op.drop_table('user')
