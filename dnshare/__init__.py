from flask import Flask, render_template
from raven.contrib.flask import Sentry
from werkzeug.contrib.fixers import ProxyFix

from .blueprints.default import default
from .blueprints.user import user
from .blueprints.user.models import User
from .extensions import db, login_manager, csrf, mail

sentry = Sentry()


def internal_server_error(error):
    return render_template('500.html'), 500


def create_app(config_override=None):
    app = Flask(__name__, instance_relative_config=True)

    if config_override is None:
        app.config.from_object('dnshare.settings')
    else:
        app.config.from_mapping(config_override)

    app.register_blueprint(default)
    app.register_blueprint(user)
    app.register_error_handler(500, internal_server_error)
    db.init_app(app)
    mail.init_app(app)
    setup_flask_login(app, User)
    csrf.init_app(app)

    environment = app.config['ENV']
    prevent_error_logging = app.config.get('LOGGING_DISABLED', False)
    if environment == 'production' and prevent_error_logging is not True:
        sentry.init_app(app)

    number_of_proxies = app.config['NUM_PROXIES']
    if number_of_proxies > 0:
        app.wsgi_app = ProxyFix(app.wsgi_app, num_proxies=number_of_proxies)

    return app


def setup_flask_login(app, user_model):
    """
    Initialize the Flask-Login extension (mutates the app passed in).

    :param app: Flask application instance
    :param user_model: Model that contains the authentication information
    :type user_model: SQLAlchemy model
    :return: None
    """
    login_manager.login_view = 'user.login'
    login_manager.login_message_category = 'info'

    @login_manager.user_loader
    def load_user(uid):
        return user_model.query.get(uid)

    login_manager.init_app(app)
