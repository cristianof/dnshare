import json

import requests

_DO_API_URL_BASE = 'https://api.digitalocean.com'


class InvalidApiKeyError(Exception):
    def __init__(self, message):
        super().__init__(message)


class DomainNotManagedError(Exception):
    def __init__(self, message):
        super().__init__(message)


class InvalidSubdomainError(Exception):
    def __init__(self, message):
        super().__init__(message)


class InvalidIPv4HostAddress(Exception):
    def __init__(self, message):
        super().__init__(message)


class DigitalOceanGateway:
    def __init__(self, api_key):
        self.api_key = api_key

    def create_a_record(self, domain, subdomain, ip):
        url = '{base}/v2/domains/{domain}/records'.format(
            base=_DO_API_URL_BASE, domain=domain)
        data = {
            'type': 'A',
            'name': subdomain,
            'data': ip,
            'priority': None,
            'port': None,
            'ttl': 3600,
            'weight': None,
            'flags': None,
            'tag': None
        }
        headers = {
            'Content-Type': 'application/json',
            'Authorization': f'Bearer {self.api_key}'
        }
        response = requests.post(url, json=data, headers=headers)
        response_dict = json.loads(response.text)

        if response.status_code == 201:
            return response_dict["domain_record"]
        else:
            _raise_according_to_error(response_dict)

    def list_all_records(self, domain):
        url = '{base}/v2/domains/{domain}/records'.format(
            base=_DO_API_URL_BASE, domain=domain)
        headers = {
            'Content-Type': 'application/json',
            'Authorization': f'Bearer {self.api_key}'
        }
        response = requests.get(url, headers=headers)
        response_dict = json.loads(response.text)

        if response.status_code == 200:
            return response_dict["domain_records"]
        else:
            _raise_according_to_error(response_dict)

    def delete_a_record(self, domain, record_id):
        url = '{base}/v2/domains/{domain}/records/{record_id}'.format(
            base=_DO_API_URL_BASE, domain=domain, record_id=record_id)
        headers = {
            'Content-Type': 'application/json',
            'Authorization': f'Bearer {self.api_key}'
        }
        response = requests.delete(url, headers=headers)
        if response.text:
            response_dict = json.loads(response.text)

        # Successful request returns empty body, so return None
        if response.status_code != 204:
            _raise_according_to_error(response_dict)


def _raise_according_to_error(response_dict):
    error_id = response_dict["id"]
    error_msg = response_dict["message"]

    if error_id == "not_found":
        raise DomainNotManagedError(error_msg)
    elif error_id == "unprocessable_entity":
        if "IPv4 format" in error_msg:
            raise InvalidIPv4HostAddress(error_msg)
        elif "valid hostname" in error_msg:
            raise InvalidSubdomainError(error_msg)
    elif error_id == "unauthorized":
        raise InvalidApiKeyError(error_msg)
    else:
        raise Exception(f'{error_id}: {error_msg}')
