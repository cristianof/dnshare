from . import create_app
from .extensions import db


def create_db():
    app = create_app()
    with app.app_context():
        db.drop_all()
        db.create_all()
