from flask_login import current_user
from flask_wtf import FlaskForm
from wtforms import HiddenField, StringField, PasswordField
from wtforms.fields.html5 import EmailField
from wtforms.validators import DataRequired, Length, Email, Optional, ValidationError
from wtforms_alchemy import Unique

from dnshare.blueprints.user.models import User
from dnshare.extensions import db
from utilities.util_wtforms import ModelForm

EMAIL_REQUIRED_MSG = "The email address is required."
PASSWORD_REQUIRED_MSG = "The password for this account is required."
PASSWORD_LEN_MSG = "Password must be equal or longer than 7 characters."


class LoginForm(FlaskForm):
    next = HiddenField()
    email = StringField('Email', [DataRequired(EMAIL_REQUIRED_MSG),
                                  Length(3, 254)])
    password = PasswordField('Password', [DataRequired(PASSWORD_REQUIRED_MSG),
                                          Length(7, message=PASSWORD_LEN_MSG)])


class SignupForm(ModelForm):
    EMAIL_ALREADY_REGISTERED_MSG = (
        "There's already an account associated to this email address. "
        "If you forgot your password, please click in \"Recover an account\".")

    email = EmailField(validators=[
        DataRequired(EMAIL_REQUIRED_MSG),
        Email(),
        Unique(
            User.email,
            get_session=lambda: db.session,
            message=EMAIL_ALREADY_REGISTERED_MSG
        )
    ])
    password = PasswordField('Password', [DataRequired(PASSWORD_REQUIRED_MSG),
                                          Length(7, message=PASSWORD_LEN_MSG)])


def _must_match_current_password(form, field):
    if not current_user.has_password(field.data):
        raise ValidationError('Invalid current password.')


def _email_not_used_by_someone_else(form, field):
    email = field.data
    email_changed = current_user.email != email
    if not email_changed:
        return

    user_already_using_email = db.session.query(User.id) \
        .filter(User.email == email, User.id != current_user.id) \
        .first()

    if user_already_using_email is not None:
        raise ValidationError("The email provided is already registered to "
                              "some other account.")


class SettingsForm(ModelForm):
    old_password = PasswordField('Current Password',
                                 [DataRequired(PASSWORD_REQUIRED_MSG),
                                  _must_match_current_password])
    new_password = PasswordField('New Password',
                                 [Optional(),
                                  Length(7, message=PASSWORD_LEN_MSG)])
    email = StringField('Email', [DataRequired(EMAIL_REQUIRED_MSG),
                                  Length(3, 254),
                                  _email_not_used_by_someone_else])


class BeginPasswordResetForm(FlaskForm):
    email = EmailField('Email', [DataRequired(EMAIL_REQUIRED_MSG),
                                 Email(),
                                 Length(3, 254)])


class ResetPasswordForm(FlaskForm):
    reset_token = HiddenField()
    password = PasswordField('New Password', [DataRequired(PASSWORD_REQUIRED_MSG),
                                              Length(7, message=PASSWORD_LEN_MSG)])
