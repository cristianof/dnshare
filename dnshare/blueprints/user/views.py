from flask import Blueprint, request, redirect, url_for, flash, render_template
from flask_login import login_user, login_required, logout_user, current_user

from dnshare.blueprints.user.application import send_password_reset_email
from dnshare.extensions import db, csrf
from .decorators import anonymous_required
from .forms import LoginForm, SignupForm, SettingsForm, BeginPasswordResetForm, ResetPasswordForm
from .models import User

user = Blueprint('user', __name__, template_folder='templates')


@user.route('/login', methods=['GET', 'POST'])
@anonymous_required()
def login():
    form = LoginForm(next=request.args.get('next'))

    if form.validate_on_submit():
        u = User.query.filter_by(email=request.form.get('email')).first()

        if u and u.has_password(request.form.get('password')):
            if login_user(u, remember=True) and u.is_active():
                u.update_activity_tracking(request.remote_addr)

                # Handle optionally redirecting to the next URL safely.
                next_url = request.form.get('next')
                if next_url:
                    return redirect(next_url)

                return redirect(url_for('default.home'))
            else:
                flash('This account has been disabled.', 'error')
        else:
            flash('Email or password is incorrect.', 'error')

    return render_template('user/login.html', form=form)


@user.route('/logout', methods=['POST'])
@login_required
@csrf.exempt
def logout():
    logout_user()
    flash('You have been logged out.', 'success')
    return redirect(url_for('user.login'))


@user.route('/signup', methods=['GET', 'POST'])
@anonymous_required()
def signup():
    form = SignupForm()

    if form.validate_on_submit():
        u = User()

        form.populate_obj(u)
        u.password = User.encrypt_password(request.form.get('password'))
        db.session.add(u)
        db.session.commit()

        if login_user(u):
            flash('Registration complete. Thanks for signing up.', 'success')
            return redirect(url_for('default.home'))

    return render_template('user/signup.html', form=form)


@user.route('/settings', methods=['GET', 'POST'])
@login_required
def settings():
    form = SettingsForm()

    if form.is_submitted():
        if form.validate():
            current_user.email = form.email.data

            new_password = form.new_password.data
            if new_password:
                current_user.password = User.encrypt_password(new_password)

            db.session.commit()

            flash('Account settings updated.', 'success')
            return redirect(url_for('user.settings'))

    else:
        form.email.data = current_user.email

    return render_template('user/settings.html', form=form)


@user.route('/forgot-password', methods=['GET', 'POST'])
def begin_password_reset():
    form = BeginPasswordResetForm()

    if form.validate_on_submit():
        email = form.email.data
        send_password_reset_email(email)

        flash('An email has been sent to {0}.'.format(email), 'success')
        return redirect(url_for('user.login'))

    return render_template('user/begin_password_reset.html', form=form)


@user.route('/reset-password', methods=['GET', 'POST'])
def reset_password():
    form = ResetPasswordForm(reset_token=request.args.get('reset_token'))

    if form.validate_on_submit():
        u = User.deserialize_token(request.form.get('reset_token'))

        if u is None:
            flash('Your reset token has expired or was tampered with.',
                  'error')
            return redirect(url_for('user.begin_password_reset'))

        u.password = User.encrypt_password(request.form.get('password'))
        db.session.commit()

        flash('Your password has been reset.', 'success')
        return redirect(url_for('user.login'))

    return render_template('user/reset_password.html', form=form)
