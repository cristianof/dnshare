from dnshare.blueprints.user.models import User
from utilities.util_flaskmail import send_template_message


def send_password_reset_email(email):
    """
    Send a password reset link to the given email, but only if the email
    is associated with an user.

    :param email: User e-mail address
    :type email: str
    """
    user = User.query.filter_by(email=email).first()
    if user is None:
        return
    reset_token = user.serialize_token()

    ctx = {'user': user, 'reset_token': reset_token}
    send_template_message(subject='DNShare.org account password reset',
                          recipients=[user.email],
                          template='user/mail/password_reset', ctx=ctx)
