import datetime

import pytz
from flask import current_app
from flask_login import UserMixin
from itsdangerous import TimedJSONWebSignatureSerializer
from sqlalchemy.orm import relationship
from werkzeug.security import generate_password_hash, check_password_hash

from dnshare.extensions import db
from utilities.util_sqlalchemy import TzAwareDateTime


class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)

    # Authentication
    email = db.Column(db.String(255), unique=True, index=True, nullable=False)
    password = db.Column(db.Text, nullable=False)
    active = db.Column('is_active', db.Boolean(), nullable=False, server_default='1')

    # Very basic activity tracking
    created_ip = db.Column(db.String(45))  # 45 allows storing IPv6 as text
    created_date = db.Column(TzAwareDateTime())
    last_sign_in_ip = db.Column(db.String(45))
    last_sign_in_date = db.Column(TzAwareDateTime())

    registrations = relationship("DomainRegistration", back_populates="user")

    def __init__(self, **kwargs):
        super(User, self).__init__(**kwargs)
        self.password = User.encrypt_password(kwargs.get('password', ''))

    def __repr__(self) -> str:
        return f'<User {self.id} {self.email}>'

    @classmethod
    def encrypt_password(cls, plaintext_password):
        """
        Hash a plaintext string using PBKDF2.

        :param plaintext_password: Password in plain text
        :type plaintext_password: str
        :return: str
        """
        if plaintext_password:
            return generate_password_hash(plaintext_password)

        return None

    def is_active(self):
        """
        Return whether or not the user account is active, this is here
        mostly to satisfy Flask-Login requirement of an is_active method.

        :return: bool
        """
        return self.active

    def has_password(self, password):
        """
        Check if the provided password matches with this users'

        :param password: The password to check
        :type password: str
        :return: bool
        """
        return password and check_password_hash(self.password, password)

    def update_activity_tracking(self, ip_address):
        """
        Update last sign-in date and IP.

        :param ip_address: IP address
        :type ip_address: str
        :return: None
        """
        self.last_sign_in_date = datetime.datetime.now(pytz.utc)
        self.last_sign_in_ip = ip_address
        db.session.commit()

    def serialize_token(self, expiration=3600):
        """
        Sign and create a token that can be used for things such as resetting
        a password or other tasks that involve a one off token.

        :param expiration: Seconds until it expires, defaults to 1 hour
        :type expiration: int
        :return: JSON
        """
        private_key = current_app.config['SECRET_KEY']

        serializer = TimedJSONWebSignatureSerializer(private_key, expiration)
        return serializer.dumps({'user_email': self.email}).decode('utf-8')

    @classmethod
    def deserialize_token(cls, token):
        """
        Obtain a user from de-serializing a signed token.

        :param token: Signed token.
        :type token: str
        :return: User instance or None
        """
        private_key = TimedJSONWebSignatureSerializer(
            current_app.config['SECRET_KEY'])
        try:
            decoded_payload = private_key.loads(token)

            return User.query \
                .filter_by(email=decoded_payload.get('user_email')) \
                .first()
        except Exception:
            return None
