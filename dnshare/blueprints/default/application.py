"""Application layer module for the default blueprint."""
import os
from collections import namedtuple
from datetime import datetime, timedelta

from blinker import Namespace
from sqlalchemy import func

from dnshare.extensions import db
from dnshare.gateways import DigitalOceanGateway
from .models import DomainRegistration


class SubdomainAlreadyTaken(Exception):
    def __init__(self, message):
        super().__init__(message)


class RegistrationLimitSurpassed(Exception):
    def __init__(self, message=None):
        super().__init__(message)


OperationResult = namedtuple(
    'OperationResult', ['result', 'data'])
_config = {
    'DIGITALOCEAN_API_KEY': os.environ['DIGITALOCEAN_API_KEY'],
    'DAILY_REGISTRATION_LIMIT': os.environ['DAILY_REGISTRATION_LIMIT']
}

# Application Events
app_signals = Namespace()
registration_overriden = app_signals.signal('registration-overriden')


def register_subdomain(subdomain, domain, host_ip, owner_ip, owner_acc=None):
    """Register a subdomain for an anonymous user.

    Args:
        subdomain (str): The subdomain to register.
        domain (str): The domain under which the subdomain will be created.
            Currently it can be either 'dnshare.org' or  'dnshare.club'.
        host_ip (str): IPv4 formatted IP address to which the
            subdomain.domain will resolve.
        owner_ip (str): IPv4 formatted IP address that'll be able to make
            further changes to this registration.
        owner_acc (User): The currently logged user, if any.

    Returns:
        OperationResult if the registration is successful, with result
        being 'ok' and data being None.

        If there was a still active registration which was overriden,
        result will be 'reregistration' and 'data' will correspond
        to the previously active host_ip.

    Raises:
        dnshare.blueprints.default.application.SubdomainAlreadyTaken: If the
            combination of subdomain and domain is already taken and currently
            active, and its owner is different from owner_ip.
        dnshare.gateways.InvalidSubdomainError: If the subdomain isn't valid.
        dnshare.gateways.DomainNotManagedError: If the domain isn't managed by
            DNShare.
        dnshare.gateways.InvalidIPv4HostAddress: If host_ip isn't a valid IPv4
            IP address.
    """
    try:
        existing_dr = DomainRegistration.query \
            .filter_by(subdomain=subdomain, domain=domain, is_active=True) \
            .one_or_none()

        # If there's an active registration from someone else, STOP.
        if existing_dr:
            dr_owner = existing_dr.user
            if dr_owner and (not owner_acc or dr_owner.id != owner_acc.id):
                raise SubdomainAlreadyTaken(f'{subdomain}.{domain}')
            elif existing_dr.creator_ip != owner_ip:
                raise SubdomainAlreadyTaken(f'{subdomain}.{domain}')

        if existing_dr is None:
            _raise_error_if_daily_registration_limit_surpassed(owner_ip)

        utc_now = datetime.utcnow()
        expiration_date_utc = utc_now + timedelta(days=7)
        registration = DomainRegistration(
            subdomain=subdomain,
            domain=domain,
            host_ip=host_ip,
            creator_ip=owner_ip,
            creation_date_utc=utc_now,
            expiration_date_utc=expiration_date_utc,
            is_active=True,
            user=owner_acc)
        db.session.add(registration)

        api_key = _config["DIGITALOCEAN_API_KEY"]
        gateway = DigitalOceanGateway(api_key)
        new_record = gateway.create_a_record(domain, subdomain, host_ip)
        registration.id = new_record["id"]

        if existing_dr is not None:
            _invalidate_registration(existing_dr)

        db.session.commit()
    except Exception:
        db.session.rollback()
        raise


def _delete_registration(domain, registration_id):
    api_key = _config["DIGITALOCEAN_API_KEY"]
    gateway = DigitalOceanGateway(api_key)
    gateway.delete_a_record(domain, registration_id)


def get_claimable_registrations(user_ip):
    return DomainRegistration.query \
        .filter_by(creator_ip=user_ip, is_active=True, user=None) \
        .all()


def get_user_registrations(user_id):
    return DomainRegistration.query \
        .filter_by(user_id=user_id, is_active=True) \
        .all()


def claim_anon_registration(user_id, user_ip, registration_id):
    try:
        claimables = get_claimable_registrations(user_ip)
        to_claim = next(x for x in claimables if x.id == registration_id)
        to_claim.user_id = user_id
        db.session.commit()
    except StopIteration:
        msg = f"User {user_id} has no permission to claim registration" \
              f" {registration_id}."
        raise PermissionError(msg)


def delete_registration(user_id, registration_id):
    to_delete = DomainRegistration.query.get(registration_id)

    if to_delete is None:
        msg = f"No registration with ID {registration_id} found."
        raise KeyError(msg)
    if to_delete.user_id != user_id:
        msg = f"User {user_id} has no permission to delete " \
              f"registration {registration_id}."
        raise PermissionError(msg)

    _delete_registration(to_delete.domain, registration_id)
    to_delete.is_active = False
    db.session.commit()


def _invalidate_registration(registration):
    _delete_registration(registration.domain, registration.id)
    registration.is_active = False
    registration_overriden.send(old_host_ip=registration.host_ip)


def _raise_error_if_daily_registration_limit_surpassed(owner_ip):
    daily_limit = int(_config["DAILY_REGISTRATION_LIMIT"])

    registrations_today = _get_todays_registration_count(owner_ip)

    if registrations_today >= daily_limit:
        raise RegistrationLimitSurpassed()


def _get_todays_registration_count(owner_ip):
    # TODO: Determinar como testear de mejor manera queries con SQLAlchemy.
    today = datetime.date(datetime.utcnow())
    return DomainRegistration.query \
        .filter_by(creator_ip=owner_ip) \
        .filter(func.DATE(DomainRegistration.creation_date_utc) == today) \
        .count()
