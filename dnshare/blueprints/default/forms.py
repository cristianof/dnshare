from flask_wtf import FlaskForm
from wtforms import StringField, SelectField, IntegerField
from wtforms.validators import DataRequired, ValidationError


class AnonRegisterForm(FlaskForm):
    SUBDOMAIN_REQUIRED_MSG = "The subdomain is required."
    DOMAIN_REQUIRED_MSG = "Selecting a domain is required."
    HOST_IP_REQUIRED_MSG = "The IP to which the name will resolve is required."

    subdomain = StringField(
        'Subdomain', validators=[DataRequired(SUBDOMAIN_REQUIRED_MSG)])
    domain = SelectField(
        'Domain', validators=[DataRequired(DOMAIN_REQUIRED_MSG)])
    host_ip = StringField(
        'Host IP', validators=[DataRequired(HOST_IP_REQUIRED_MSG)])

    def validate_subdomain(form, field):
        # We have to check for protected subdomains too.
        if field.data in ["www", "@"]:
            raise ValidationError("protected_subdomain")


class ClaimForm(FlaskForm):
    registration_id = IntegerField('Registration ID',
                                   validators=[DataRequired()])


class DeleteForm(FlaskForm):
    registration_id = IntegerField('Registration ID',
                                   validators=[DataRequired()])
