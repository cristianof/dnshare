from flask import Blueprint, render_template, make_response, flash, \
    redirect, url_for, request
from flask_login import current_user, login_required

from dnshare.gateways import InvalidIPv4HostAddress, InvalidSubdomainError
from .application import register_subdomain, registration_overriden, \
    SubdomainAlreadyTaken, RegistrationLimitSurpassed, \
    get_claimable_registrations, get_user_registrations, claim_anon_registration, \
    delete_registration
from .forms import AnonRegisterForm, ClaimForm, DeleteForm

default = Blueprint('default', __name__, template_folder='templates')

SUBMIT_ERROR_MSG = ('It appears that there are some errors with your '
                    'registration. Please check and try again.')
REGISTRATION_OK_TPL = ('{subdomain}.{domain} was successfully '
                       'registered to resolve to {host_ip}.')
TOO_MANY_REGISTRATIONS_MSG = 'You have registered quite a few domains today. Please try again tomorrow.'
NGGYU_URL = 'https://www.youtube.com/watch?v=dQw4w9WgXcQ'
INVALID_HOST_TPL = ('The host "{host}" apparently is not a valid IPv4. '
                    'Please check that and try again.')
INVALID_SUBDOMAIN_TPL = ('The subdomain "{subdomain}" doesn\'t appear to be '
                         'a valid subdomain. Please check that and try again.')


@default.route("/", methods=['GET'])
def home():
    form = _create_anon_register_form()
    remote_ip = request.remote_addr

    return render_template('default/home.html', form=form, remote_ip=remote_ip)


@default.route('/', methods=['POST'])
def register_anon_host():
    form = _create_anon_register_form()
    if not form.validate():
        if "protected_subdomain" in form.subdomain.errors:
            return redirect(NGGYU_URL)

        home_page = render_template('default/home.html', form=form)
        return make_response(home_page, 400)

    subdomain = form.subdomain.data
    domain = form.domain.data
    host_ip = form.host_ip.data
    creator_ip = request.environ.get('HTTP_X_REAL_IP', request.remote_addr)

    def handle_reregistration(sender, old_host_ip):
        msg = _create_override_msg(subdomain, domain, old_host_ip, host_ip)
        flash(msg, 'info')

    registration_overriden.connect(handle_reregistration)

    try:
        is_anon = current_user.is_anonymous
        register_subdomain(subdomain, domain, host_ip, creator_ip,
                           None if is_anon else current_user)
    except SubdomainAlreadyTaken:
        taken_msg = _create_already_taken_msg(subdomain, domain)
        flash(taken_msg, 'error')
        return redirect(url_for('default.home'))
    except RegistrationLimitSurpassed:
        flash(TOO_MANY_REGISTRATIONS_MSG, 'error')
        return redirect(url_for('default.home'))
    except InvalidIPv4HostAddress:
        host_error = _create_invalid_host_msg(host_ip)
        flash(host_error, 'error')
        return redirect(url_for('default.home'))
    except InvalidSubdomainError:
        error = _create_invalid_subdomain_msg(subdomain)
        flash(error, 'error')
        return redirect(url_for('default.home'))

    success_message = _create_success_msg(
        form.subdomain.data, form.domain.data, form.host_ip.data)
    flash(success_message, 'success')

    return redirect(url_for('default.home'))


@default.route('/registrations', methods=['GET'])
@login_required
def registrations():
    user_ip = request.environ.get('HTTP_X_REAL_IP', request.remote_addr)
    user_id = current_user.id

    claimable_registrations = get_claimable_registrations(user_ip)
    active_registrations = get_user_registrations(user_id)

    return render_template('default/my_domains.html',
                           claimable=claimable_registrations,
                           active=active_registrations)


@default.route('/claim', methods=['POST'])
@login_required
def claim():
    user_ip = request.environ.get('HTTP_X_REAL_IP', request.remote_addr)
    user_id = current_user.id

    form = ClaimForm()
    if form.validate():
        claim_anon_registration(user_id, user_ip, form.registration_id.data)

    flash('Registration has been claimed successfully!', 'success')
    return redirect(url_for('default.registrations'))


@default.route('/delete', methods=['POST'])
@login_required
def delete():
    user_id = current_user.id

    form = DeleteForm()
    if form.validate():
        delete_registration(user_id, form.registration_id.data)

    flash('Registration has been deleted. Change may take up to 1 hour '
          'to propagate to DNS servers due to DNS caching.', 'success')
    return redirect(url_for('default.registrations'))


@default.route('/about', methods=['GET'])
def about():
    return render_template('default/about.html')


@default.route('/privacy', methods=['GET'])
def privacy():
    return render_template('default/privacy.html')


def _create_anon_register_form():
    form = AnonRegisterForm()
    form.domain.choices = [(d, d) for d in _get_available_domains()]
    return form


def _get_available_domains():
    """Get all available domains for which a user can register a
    subdomain. Currently hardcoded."""
    return ["dnshare.org"]


def _create_success_msg(subdomain, domain, host_ip):
    return REGISTRATION_OK_TPL.format(**locals())


def _create_override_msg(subdomain, domain, old_ip, new_ip):
    return (f'{subdomain}.{domain} updated! Previously it resolved to '
            f'{old_ip}, now it resolves to {new_ip}.')


def _create_already_taken_msg(subdomain, domain):
    return (f'{subdomain}.{domain} is already registered and active. '
            'Please choose other subdomain and domain combination.')


def _create_invalid_host_msg(host_ip):
    return INVALID_HOST_TPL.format(host=host_ip)


def _create_invalid_subdomain_msg(subdomain):
    return INVALID_SUBDOMAIN_TPL.format(subdomain=subdomain)
