from sqlalchemy import ForeignKey
from sqlalchemy.orm import relationship

from dnshare.extensions import db


class DomainRegistration(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    subdomain = db.Column(db.String, nullable=False)
    domain = db.Column(db.String, nullable=False)
    host_ip = db.Column(db.String(15), nullable=False)
    creator_ip = db.Column(db.String(15), nullable=False)
    creation_date_utc = db.Column(db.DateTime, nullable=False)
    expiration_date_utc = db.Column(db.DateTime, nullable=False)
    is_active = db.Column(db.Boolean, nullable=False)

    user_id = db.Column(db.Integer, ForeignKey('user.id'), nullable=True)
    user = relationship("User", back_populates="registrations")

    def __repr__(self):
        return (f'<DomainRegistration {self.id} {self.subdomain}.{self.domain}'
                f' => {self.host_ip} by {self.creator_ip}>')
