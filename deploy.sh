#!/bin/sh
# This is an example deployment script apt for personal projects deployed in a single
# remote host. It's most likely not appropiate for production deployments.
cd dnshare
docker image pull sinenie/dnshare:latest
docker-compose -f dnshare.docker-compose.yml down
docker-compose -f dnshare.docker-compose.yml up -d
