NAME   := ${DOCKER_USER}/dnshare
TAG    := $$(git log -1 --pretty=%h)
IMG    := ${NAME}:${TAG}
LATEST := ${NAME}:latest
SERVER := ${SSH_USER}@${SSH_HOST}

build:
	@yarn build
	@docker image build -t ${IMG} -t ${LATEST} .

push:
	@docker push ${NAME}

deploy:
	@scp dnshare.docker-compose.yml ${SERVER}:dnshare/
	@cat deploy.sh | ssh ${SERVER}

login:
	@docker login -u ${DOCKER_USER} -p ${DOCKER_PASS}

test:
	.venv/bin/pytest -v

ptw:
	.venv/bin/ptw --nobeep --clear --spool 100 --runner ".venv/bin/pytest -v -c pytest.unit.ini"

ft:
	.venv/bin/pytest -v -c pytest.functional.ini
