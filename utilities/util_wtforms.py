from flask_wtf import FlaskForm


class ModelForm(FlaskForm):
    """
    wtforms_components exposes ModelForm but their ModelForm does not inherit
    from flask_wtf's Form, but instead WTForm's Form.

    However, in order to get CSRF protection handled by default we need to
    inherit from flask_wtf's Form. So let's just copy his class directly.

    We modified it by removing the format argument so that wtforms_component
    uses its own default which is to pass in request.form automatically.
    """

    # noinspection PyCallByClass
    def __init__(self, obj=None, prefix='', **kwargs):
        FlaskForm.__init__(
            self, obj=obj, prefix=prefix, **kwargs
        )
        self._obj = obj
