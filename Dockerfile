FROM python:3.6.5-slim

ENV INSTALL_PATH /app

RUN mkdir -p ${INSTALL_PATH}
VOLUME ${INSTALL_PATH}/instance

WORKDIR ${INSTALL_PATH}

COPY . .
RUN pip install -r requirements/prod.txt

CMD gunicorn -b 0.0.0.0:8966 --access-logfile - --error-logfile - "dnshare:create_app()"
