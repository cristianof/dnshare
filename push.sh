#!/bin/bash
NAME="registry.gitlab.com/cristianof/dnshare"
TAG=$(git log -1 --pretty=%h)
IMG="$NAME:$TAG"
LATEST="$NAME:latest"

docker image push $IMG &&
docker image push $LATEST 

