import unittest.mock
import pytest
import dnshare.gateways


@pytest.fixture
def gateway():
    return dnshare.gateways.DigitalOceanGateway('fake_api_key')


def test_successful_operation_returns_record_info(gateway, mocker):
    # Arrange
    response = _create_fake_response(201, '''{
        "domain_record": {
            "id": 1,
            "type": "A",
            "name": "test",
            "data": "127.0.0.1",
            "priority": null,
            "port": null,
            "ttl": 3600,
            "weight": null,
            "flags": null,
            "tag": null
        }
    }''')
    mocker.patch("requests.post", return_value=response)

    # Act
    record_info = gateway.create_a_record('example.com', 'test', '127.0.0.1')

    # Assert
    assert record_info["id"] == 1
    assert record_info["type"] == "A"
    assert record_info["name"] == "test"
    assert record_info["data"] == "127.0.0.1"
    assert record_info["priority"] is None
    assert record_info["port"] is None
    assert record_info["ttl"] == 3600
    assert record_info["weight"] is None
    assert record_info["flags"] is None
    assert record_info["tag"] is None


def test_domain_not_managed_raises_exception(gateway, mocker):
    # Arrange
    response = _create_fake_response(404, '''{
        "id": "not_found",
        "message": "The resource you were accessing could not be found."
    }''')
    mocker.patch('requests.post', return_value=response)

    # Act & assert
    with pytest.raises(dnshare.gateways.DomainNotManagedError):
        gateway.create_a_record('example.com', 'test', '127.0.0.1')


def test_invalid_subdomain_raises_exception(gateway, mocker):
    # Arrange
    response = _create_fake_response(422, '''{
        "id": "unprocessable_entity",
        "message": "Name Only valid hostname characters are allowed. (a-z, A-Z, 0-9, ., _ and -) Or a single record of '@'."
    }''')  # noqa
    mocker.patch('requests.post', return_value=response)

    # Act & assert
    with pytest.raises(dnshare.gateways.InvalidSubdomainError):
        gateway.create_a_record('example.com', 'tes/t', '127.0.0.1')


def test_invalid_host_ip_raises_exception(gateway, mocker):
    # Arrange
    response = _create_fake_response(422, '''{
        "id": "unprocessable_entity",
        "message": "IP address did not match IPv4 format (e.g. 127.0.0.1)."
    }''')
    mocker.patch('requests.post', return_value=response)

    # Act & assert
    with pytest.raises(dnshare.gateways.InvalidIPv4HostAddress):
        gateway.create_a_record('example.com', 'test', 'this-is-not-an-ip')


def _create_fake_response(status_code, text):
    mock = unittest.mock.MagicMock
    mock.status_code = status_code
    mock.text = text
    return mock
