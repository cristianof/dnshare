import re
import html
from urllib.parse import urlparse
import pytest
from bs4 import BeautifulSoup
from dnshare import create_app, gateways
from dnshare.blueprints.default import views, forms, application


def test_home_page_route(client):
    # Arrange & Act
    response = client.get('/')

    # Assert
    assert response.status_code == 200


def test_anon_register_form_shown(client):
    # Arrange & Act
    response = client.get('/')

    # Assert
    soup = BeautifulSoup(response.data, 'html.parser')
    register_form = soup.find(id='anon-register-form')

    assert register_form is not None
    assert register_form.find(attrs={'name': 'subdomain'}) is not None
    assert register_form.find(attrs={'name': 'domain'}) is not None
    assert register_form.find(attrs={'name': 'host_ip'}) is not None


def test_available_domains_shown(mocker, client):
    # Arrange
    mocker.patch.object(
        views,
        '_get_available_domains',
        return_value=['dnshare.org', 'dnshare.club'])

    # Act
    response = client.get('/')

    # Assert
    soup = BeautifulSoup(response.data, 'html.parser')
    domain_select = soup.find(attrs={'name': 'domain'})
    options = domain_select.find_all('option')

    assert len(options) == 2
    assert domain_select.find(
        string="dnshare.org", value="dnshare.org") is not None
    assert domain_select.find(
        string="dnshare.club", value="dnshare.club") is not None


def test_empty_form_submit_returns_error(mocker, client):
    # Act
    response = client.post('/', data={})

    # Assert
    assert response.status_code == 400


@pytest.mark.parametrize('data,expected_message', [({
    'subdomain': '',
    'domain': 'dnshare.org',
    'host_ip': '127.0.0.1'
}, forms.AnonRegisterForm.SUBDOMAIN_REQUIRED_MSG), ({
    'subdomain': 'example',
    'domain': '',
    'host_ip': '127.0.0.1'
}, forms.AnonRegisterForm.DOMAIN_REQUIRED_MSG), ({
    'subdomain': 'example',
    'domain': 'dnshare.org',
    'host_ip': ''
}, forms.AnonRegisterForm.HOST_IP_REQUIRED_MSG)])
def test_individual_field_errors_shown(client, data, expected_message):
    # Act
    response = client.post('/', data=data)

    # Assert
    assert expected_message in str(response.data, 'utf-8')


def test_valid_submit_saves_triggers_domain_registration(client, mocker):
    # Arrange
    mocked_register = mocker.patch.object(views, "register_subdomain")

    # Act
    _post_successful_registration(client)

    # Assert
    mocked_register.assert_called()


def test_valid_submit_redirects_to_home_page(client, mocker):
    # Arrange
    mocker.patch.object(views, "register_subdomain")
    expected_path = '/'

    # Act
    response = _post_successful_registration(client)

    # Assert
    assert response.status_code == 302
    assert urlparse(response.location).path == expected_path


def test_valid_submit_shows_success_message_after_redirect(client, mocker):
    # Arrange
    mocker.patch.object(views, "register_subdomain")
    expected = views._create_success_msg('example', 'dnshare.org', '127.0.0.1')

    # Act
    response = _post_successful_registration(client, follow_redirects=True)

    # Assert
    assert response.status_code == 200
    assert expected in str(response.data, 'utf-8')


def test_after_registration_override_message_is_shown(client, mocker):
    # Arrange
    def fire_overriden_signal(*args, **kwargs):
        application.registration_overriden.send(old_host_ip='192.168.0.1')

    mocker.patch.object(views, "register_subdomain", new=fire_overriden_signal)
    expected = views._create_override_msg(
        'example', 'dnshare.org', '192.168.0.1', '127.0.0.1')

    # Act
    response = _post_successful_registration(client, follow_redirects=True)

    # Assert
    assert response.status_code == 200
    assert expected in str(response.data, 'utf-8')


def test_override_msg_shown_only_once_after_multiple_overrides(client, mocker):
    # Arrange
    def fire_overriden_signal(*args, **kwargs):
        application.registration_overriden.send(old_host_ip='192.168.0.1')

    mocker.patch.object(views, "register_subdomain", new=fire_overriden_signal)

    # Act
    _post_successful_registration(client, follow_redirects=True)
    _post_successful_registration(client, follow_redirects=True)
    response = _post_successful_registration(client, follow_redirects=True)

    # Assert
    assert response.status_code == 200
    matches = re.findall("now it resolves to", str(response.data, 'utf-8'))
    assert len(matches) == 1


def test_error_shown_if_subdomain_already_taken(client, mocker):
    # Arrange
    expected_ex = application.SubdomainAlreadyTaken('example.dnshare.org')
    mocker.patch.object(views, "register_subdomain",
                        side_effect=expected_ex)
    expected_msg = views._create_already_taken_msg('example', 'dnshare.org')

    # Act
    response = _post_successful_registration(client, follow_redirects=True)

    # Assert
    assert expected_msg in str(response.data, 'utf-8')


def test_error_shown_after_too_many_registrations(client, mocker):
    # Arrange
    expected_ex = application.RegistrationLimitSurpassed()
    mocker.patch.object(views, "register_subdomain", side_effect=expected_ex)
    rate_limited_msg = views.TOO_MANY_REGISTRATIONS_MSG

    # Act
    rate_limited_response = _post_successful_registration(client, True)

    # Assert
    assert rate_limited_msg in str(rate_limited_response.data, 'utf-8')


def test_correct_public_ip_is_shown_if_not_behind_proxy(client):
    # Act
    response = client.get('/')

    # Assert
    assert 'IP: 127.0.0.1' in str(response.data, 'utf-8')


def test_correct_public_ip_is_shown_if_behind_reverse_proxy(config):
    # We consider the request reverse proxied if it contains the
    # X-Forwarded-For HTTP header. Of course, if the application isn't
    # behind a proxy, but the config says it is, then a malicious user
    # could forge such header and then GG...
    # Arrange
    config['NUM_PROXIES'] = 1
    app = create_app(config)
    client = app.test_client()
    headers = {
        'X-Forwarded-For': '1.2.3.4',
        'X-Real-IP': '1.2.3.4',
        'X-Forwarded-Proto': 'https'
    }

    # Act
    response = client.get('/', headers=headers)

    # Assert
    assert 'IP: 1.2.3.4' in str(response.data, 'utf-8')


def test_invalid_api_error_handled(config, mocker):
    """Test whether the generic server error page is shown.

    A Invalid API error should never happen, as it denotes that the
    application was incorrectly configured. We show the user a generic
    error page and log the error.
    """
    # Arrange
    mock_gw = mocker.MagicMock()
    mock_gw.create_a_record.side_effect = gateways.InvalidApiKeyError('')
    mocker.patch.object(application, "DigitalOceanGateway", return_value=mock_gw)

    mock_dr = mocker.MagicMock()
    mock_dr.query.filter_by().one_or_none.return_value = None
    mocker.patch.object(application, 'DomainRegistration', new=mock_dr)

    mocker.patch.object(application,
                        "_raise_error_if_daily_registration_limit_surpassed")
    mocker.patch.object(application, "db")

    # Hack to test custom error pages, but don't log errors to Sentry
    config['TESTING'] = False
    config['ENV'] = 'production'
    config['DEBUG'] = False
    config['LOGGING_DISABLED'] = True
    app = create_app(config)
    client = app.test_client()

    # Act
    response = _post_successful_registration(client, follow_redirects=True)

    # Assert
    assert response.status_code == 500
    assert 'HTTP 500' in str(response.data, 'utf-8')


def test_error_shown_after_invalid_host_ip_submitted(client, mocker):
    # Arrange
    host = 'wtf???'
    data = {
        'subdomain': 'example',
        'domain': 'dnshare.org',
        'host_ip': host
    }
    ex = gateways.InvalidIPv4HostAddress('')
    mocker.patch.object(views, 'register_subdomain', side_effect=ex)
    expected_error = views._create_invalid_host_msg(host)

    # Act
    response = client.post('/', data=data, follow_redirects=True)

    # Assert
    assert expected_error in html.unescape(str(response.data, 'utf-8'))


def test_reregistration_successful_msg_not_shown_if_it_was_not(client, mocker):
    """Regression test for bug were we invalidated record during reregistration

    Basically, what happened was the following:
    1. User registers valid subdomain with valid host IP.
    2. User tries to reregister that same subdomain, but enters an invalid
       host IP.
    3. System deletes the existing registration from DigitalOcean, and triggers
       the "re-registration successful" message.
    4. System tries to register the new domain -> host_ip record.
    5. Exception is raised because host IP is invalid, rollback is performed
       in runnning DB transaction.
    6. User gets to see both the "invalid host IP" error and "successful
       registration" message at the same time. Wonderful.

    But what's worse is actually that due to the successful deletion of
    the original registration from DigitalOcean, but not locally, then any
    attempt from re-registering the subdomain (even with a valid host IP) will
    fail upon trying to re-delete such registration.

    What was done to fix this was to leave the "old" registration invalidation
    to AFTER the new registration finishes successfully.
    """
    # Arrange & Act
    invalid_data = {
        'subdomain': 'example',
        'domain': 'dnshare.org',
        'host_ip': 'wtf???'
    }

    # 1. There's an already valid registration.
    existing_dr = mocker.MagicMock()
    existing_dr.creator_ip = '1.2.3.4'

    mock_dr = mocker.MagicMock()
    mock_dr.query.filter_by().one_or_none.return_value = existing_dr
    mocker.patch.object(application, 'DomainRegistration', new=mock_dr)

    # 3. System deletes the existing registration
    # We'll, we don't care about the deletion, but mostly about the success
    # message.
    mocker.patch.object(application, 'delete_registration')

    # 4 & 5. Exception is raised because host IP is invalid.
    expected_ip_ex = gateways.InvalidIPv4HostAddress('')
    mock_gw = mocker.MagicMock()
    mock_gw.create_a_record.side_effect = expected_ip_ex
    mocker.patch.object(application, 'DigitalOceanGateway',
                        return_value=mock_gw)

    # 2. User tries to submit an invalid re-registration
    response = client.post('/', data=invalid_data, follow_redirects=True)

    # Assert
    # 6. BUT THIS TIME THE SUCCESS MESSAGE IS NOT SHOWN!
    assert 'updated!' not in str(response.data, 'utf-8')


def test_error_shown_after_invalid_subdomain_submitted(client, mocker):
    # Arrange
    subdomain = 'not//quite-%right'
    data = {
        'subdomain': subdomain,
        'domain': 'dnshare.org',
        'host_ip': '127.0.0.1'
    }
    ex = gateways.InvalidSubdomainError('')
    mocker.patch.object(views, 'register_subdomain', side_effect=ex)
    expected_error = views._create_invalid_subdomain_msg(subdomain)

    # Act
    response = client.post('/', data=data, follow_redirects=True)

    # Assert
    assert expected_error in html.unescape(str(response.data, 'utf-8'))


def _post_successful_registration(client, follow_redirects=False):
    data = {
        'subdomain': 'example',
        'domain': 'dnshare.org',
        'host_ip': '127.0.0.1'
    }
    return client.post('/', data=data, follow_redirects=follow_redirects)
