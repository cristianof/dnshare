import pytest
from dnshare.blueprints.default import views, forms, application


# noinspection PyProtectedMember
def test_application_raises_error_after_registration_limit_surpassed(mocker):
    # Arrange
    owner_ip = '127.0.0.1'
    config = {'DAILY_REGISTRATION_LIMIT': str(10)}
    mocker.patch.object(application, "_get_todays_registration_count",
                        return_value=10)
    mocker.patch.object(application, "_config", new=config)

    # Act & Assert
    with pytest.raises(application.RegistrationLimitSurpassed):
        application._raise_error_if_daily_registration_limit_surpassed(
            owner_ip)
