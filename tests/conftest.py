import pytest
from dnshare import create_app


@pytest.fixture
def config(tmpdir):
    tmpdb = tmpdir.join("temp.db")
    return {
        'TESTING': True,
        'SECRET_KEY': 'dev',
        'WTF_CSRF_ENABLED': False,  # CSRF protection is tested in FT
        'SQLALCHEMY_DATABASE_URI': f'sqlite:///{tmpdb}',
        'SQLALCHEMY_TRACK_MODIFICATIONS': True,
        'DAILY_REGISTRATION_LIMIT': 10,
        'NUM_PROXIES': 0
    }


@pytest.fixture
def app(config):
    app = create_app(config)
    return app


@pytest.fixture
def client(app):
    return app.test_client()


# Workaround to raise exceptions inside lambda expressions.
# Python by default doesn't support using raise directly inside one.
@pytest.fixture
def exraise():
    def _raise(ex):
        raise ex

    return _raise
