"""Functional tests for "Register an A host subdomain anonymously"

An anonymous user is anyone who hasn't logged in, regardless of whether
they have an account or not in DNShare. They can register a subdomain
with any of our shared domains and tie it to whatever IPv4 they want.

Basic flow summary:
1. Anon visits the home page
2. The system provides him with the option of registering a subdomain in
   one of the available shared domains.
3. Anon enters his desired subdomain, selects a shared domain, and
   enters the IPv4 where the subdomain will point. He submits his
   choices.
4. The system creates the A host record successfully.
5. The system notifies the user of the successful operation.

Alternate flows:
- Anon tries to register a protected subdomain (e.g. www).
- Anon tries to register a subdomain, but it's already registered and
  still valid...
    - ... but he was the one who registered.
    - ... but someone else registered the domain.
- Anon tries to register an invalid subdomain (e.g. gibberish).
"""
from selenium import webdriver
from selenium.webdriver.support.ui import Select
import pytest
from dnshare.blueprints.default import views
from dnshare.initdb import create_db


@pytest.fixture()
def browser():
    driver = webdriver.Chrome()
    driver.implicitly_wait(3)
    yield driver
    driver.quit()


def test_success_scenario(browser):
    # Anon visits the home page
    browser.get('http://localhost:5000')

    # The system provides him with the option of registering a subdomain
    # in one of the available shared domains.
    register_form = browser.find_element_by_id('anon-register-form')

    # Anon enters his desired subdomain
    subdomain_input = register_form.find_element_by_name('subdomain')
    subdomain_input.send_keys('example')

    # ... selects a shared domain ...
    domain_select_element = register_form.find_element_by_name('domain')
    domain_select = Select(domain_select_element)
    domain_select.select_by_visible_text("dnshare.org")

    # ... and enters a IPv4 where the subdomain will point.
    target_ip_input = register_form.find_element_by_name('host_ip')
    target_ip_input.send_keys('127.0.0.1')

    # He submits his choices.
    register_form.submit()

    # The system notifies the user of the successful operation
    body_element = browser.find_element_by_tag_name('body')
    page_text = body_element.text

    # TODO: Brittle assertions, fix later(tm).
    assert "success" in page_text


@pytest.mark.parametrize('subdomain', ['www', '@'])
@pytest.mark.parametrize('domain', ['dnshare.org', 'dnshare.club'])
def test_protected_subdomain_registration(browser, subdomain, domain):
    # Anon visits the home page
    browser.get('http://localhost:5000')

    # The system provides him with the option of registering a subdomain
    # in one of the available shared domains
    register_form = browser.find_element_by_id('anon-register-form')

    # The cheeky anon tries to register a protected subdomain to mess
    # with other users
    subdomain_input = register_form.find_element_by_name('subdomain')
    subdomain_input.send_keys(subdomain)

    # He tries to do so with several shared domains
    domain_select_element = register_form.find_element_by_name('domain')
    domain_select = Select(domain_select_element)
    domain_select.select_by_visible_text(domain)

    # And with a grin, he submits his choices.
    register_form.submit()

    # But behold! For the epic trole was not so epic after all, and
    # a rickroll was all he got for his efforts.
    assert browser.current_url == views.NGGYU_URL


def test_subdomain_already_registered_by_anon(browser):
    # (Re-create DB for clean test execution)
    create_db()

    # Anon visits the home page
    browser.get('http://localhost:5000')

    # Registers a subdomain successfully
    _register_subdomain(browser, 'example', 'dnshare.org', '192.168.0.1')

    # At some other point in time, he comes back
    browser.get('http://localhost:5000')

    # And registers the same subdomain again
    _register_subdomain(browser, 'example', 'dnshare.org', '127.0.0.1')

    # And he gets a informational message telling him of what happened
    body_element = browser.find_element_by_tag_name('body')
    page_text = body_element.text

    # Sample expected message:
    # **example.dnshare.org** updated! Previously it resolved to
    # **192.168.0.1**, now it resolves to **127.0.0.1**.
    assert 'example.dnshare.org updated' in page_text
    assert 'Previously it resolved to 192.168.0.1' in page_text
    assert 'now it resolves to 127.0.0.1' in page_text


def _register_subdomain(browser, subdomain, domain, host_ip):
    # The system provides him with the option of registering a subdomain
    # in one of the available shared domains
    register_form = browser.find_element_by_id('anon-register-form')

    # Enter desired subdomain...
    subdomain_input = register_form.find_element_by_name('subdomain')
    subdomain_input.send_keys(subdomain)

    # ... domain ...
    domain_select_element = register_form.find_element_by_name('domain')
    domain_select = Select(domain_select_element)
    domain_select.select_by_visible_text(domain)

    # ... and host IP (v4)
    target_ip_input = register_form.find_element_by_name('host_ip')
    target_ip_input.send_keys(host_ip)

    # Submit the registration.
    register_form.submit()
