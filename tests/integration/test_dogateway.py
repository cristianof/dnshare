import os
import socket
import uuid
import pytest
import dnshare.gateways


def test_can_resolve_domain_after_registration():
    # Arrange
    gateway = _create_gateway()
    subdomain = uuid.uuid4().hex

    # Act
    gateway.create_a_record('dnshare.org', subdomain, '127.0.0.1')

    # Assert
    assert socket.gethostbyname(f'{subdomain}.dnshare.org') == '127.0.0.1'


def test_list_all_records_returns_something():
    # Arrange
    gateway = _create_gateway()

    # Act
    domains = gateway.list_all_records('dnshare.org')

    # Assert
    assert len(domains) > 0
    assert any(x for x in domains if x["name"] == "www")


def test_delete_a_record_actually_deletes_it():
    # We can't just resolve the address and then check for it not
    # resolving, as due to DNS caching and propagation delays this is
    # not reliable and would most likely fail (that's "resolving even as
    # the registration was deleted").
    # Arrange
    gateway = _create_gateway()
    domain = 'dnshare.org'
    subdomain = uuid.uuid4().hex
    record = gateway.create_a_record(domain, subdomain, '127.0.0.1')
    record_id = record["id"]

    # Act
    gateway.delete_a_record(domain, record_id)

    # Assert
    with pytest.raises(socket.gaierror):
        socket.gethostbyname(f'{subdomain}.{domain}')


def _create_gateway():
    api_key = os.environ['DIGITALOCEAN_API_KEY']
    return dnshare.gateways.DigitalOceanGateway(api_key)
